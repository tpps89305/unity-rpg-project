# Unity RPG Demo
![Demo1](./readme/demo1.png)
這個專案是利用 Unity 製作 RPG 遊戲的技術示範。包含 2 個簡單的場景（Scene）來實作遊戲的基本功能。

## 開發工具
Unity 版本：2019.4.9f1

程式碼編輯器：Visual Studio Code（並安裝 Unity 延伸套件）

## 閱覽方式
![讀取村莊場景1](./readme/load_village_scene.png)

在 Unity 編輯器中，雙擊 Assets > Scenes > Village 中的 Village 檔案，就能進入村莊場景。

![讀取小徑場景1](./readme/lane_scene.png)

另有 Assets > Scenes > Lane 中的小徑場景供玩家前往。

## 技術細節
- 利用 NavMesh 及 NavMesh Agent 實作地面導航功能
- 用滑鼠控制玩家（Player）移動、撿捨物品、與其他角色互動。
- 敵對角色透過 AI Controller 在巡邏區域（PatrolPath）間移動，並在玩家靠進或是被攻擊時採取行動。
- 利用 Cinemachine 製作過場動畫。
- 角色的屬性管理：生命值（Health）、經驗值（Experience）。
- 遊戲進度存檔：SavingSystem

## 已知問題
此專案有相容性問題，嘗試升級 Unity 版本到 2020.3 的版號後，會發生角色錯位、動畫不見、無法和其他角色互動的問題。