﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Attributes
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField]
        Health health = null;
        [SerializeField]
        RectTransform currentHealthBar = null;
        [SerializeField]
        Canvas rootCanvas = null;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            // approximately: 大約。使單精度浮點數正確地進行比較。
            rootCanvas.enabled = !(Mathf.Approximately(health.GetFraction(), 0) || Mathf.Approximately(health.GetFraction(), 1));
            currentHealthBar.localScale = new Vector3(health.GetFraction(), 1, 1);
        }
    }
}
