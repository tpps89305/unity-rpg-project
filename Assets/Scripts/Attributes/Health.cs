﻿using UnityEngine;
using System.Collections;
using RPG.Saving;
using RPG.Stats;
using RPG.Core;
using System;
using GameDevTV.Utils;
using UnityEngine.Events;

namespace RPG.Attributes
{
    public class Health : MonoBehaviour, ISaveable
    {
        private bool isDead = false;
        [Range(0.1f, 1f)]
        public float regenerationPercentage = 0.5f;
        [SerializeField]
        TakeDamageEvent takeDamage;
        [SerializeField]
        UnityEvent onDie;

        [System.Serializable]
        public class TakeDamageEvent : UnityEvent<float>
        {

        }

        BaseStats baseStats;

        LazyValue<float> healthPoint;

        private void Awake()
        {
            baseStats = GetComponent<BaseStats>();
            healthPoint = new LazyValue<float>(GetInitialHealth);
        }

        private float GetInitialHealth()
        {
            return baseStats.GetStat(Stat.Health);
        }

        private void OnEnable()
        {
            if (baseStats != null)
            {
                baseStats.onLevelUp += RegenerateHealth;
            }
        }

        internal void Heal(float healthToRestore)
        {
            healthPoint.value = Mathf.Min(healthPoint.value + healthToRestore, GetMaxHealthPoints());
        }

        private void OnDisable()
        {
            if (baseStats != null)
            {
                baseStats.onLevelUp -= RegenerateHealth;
            }
        }

        void Start()
        {
            healthPoint.ForceInit();
        }

        private void RegenerateHealth()
        {
            float maxHealth = baseStats.GetStat(Stat.Health);
            float regenHealthPoint = maxHealth * regenerationPercentage;
            healthPoint.value = Mathf.Min(healthPoint.value + regenHealthPoint, maxHealth);
        }

        public void TakeDamage(GameObject instigator, float damage)
        {
            healthPoint.value = Mathf.Max(healthPoint.value - damage, 0);
            Debug.Log(gameObject.name + " 的生命值尚餘 " + healthPoint);

            if (healthPoint.value <= 0)
            {
                onDie.Invoke();
                Die();
                AwardExperience(instigator);
            }
            else
            {
                takeDamage.Invoke(damage);
            }
        }

        private void AwardExperience(GameObject instigator)
        {
            Experience experience = instigator.GetComponent<Experience>();
            if (experience == null) return;

            experience.GainExperience(baseStats.GetStat(Stat.ExperienceReward));
        }

        public float GetHealthPoints()
        {
            return healthPoint.value;
        }

        public float GetMaxHealthPoints()
        {
            return baseStats.GetStat(Stat.Health);
        }

        /// <summary>
        /// 得知現有生命值的百分比
        /// </summary>
        /// <returns>「現有生命值／最大生命值。」換算成百分比</returns>
        public float GetPercentage()
        {
            return 100 * GetFraction();
        }

        public float GetFraction()
        {
            return healthPoint.value / baseStats.GetStat(Stat.Health);
        }

        public float GetHealthPoint()
        {
            return healthPoint.value;
        }

        public bool IsDead()
        {
            return healthPoint.value <= 0;
        }

        /// <summary>
        /// 使角色進入陣亡狀態
        /// </summary>
        public void Die()
        {
            if (isDead) return;
            isDead = true;
            GetComponent<Animator>().SetTrigger("die");
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        public object CaptureState()
        {
            return healthPoint.value;
        }

        public void RestoreState(object state)
        {
            healthPoint.value = (float) state;
            if (healthPoint.value <= 0)
            {
                Die();
            }
        }
    }
}