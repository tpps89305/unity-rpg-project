﻿using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Core;
using RPG.Attributes;
using UnityEngine;

namespace RPG.Combat
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Weapons/Make New Weapon", order = 0)]
    public class WeaponConfig : ScriptableObject
    {
        [Header("要裝備的武器")]
        [SerializeField]
        Weapon equippedPrefab = null;
        [SerializeField]
        AnimatorOverrideController animatorOverride = null;
        [SerializeField]
        float weaponDamage = 10f;
        [Header("武器加成：輸入 10 代表提升 10%")]
        [SerializeField]
        float percentageBonus = 0;
        [SerializeField]
        float weaponRange = 3f;
        [SerializeField]
        bool isRightHanded = true;
        [SerializeField]
        Projectile projectile = null;

        const string weaponName = "Weapon";

        public Weapon Spawn(Transform rightHand, Transform leftHand, Animator animator)
        {
            DestroyOldWeapon(rightHand, leftHand);

            Weapon weapon = null;
            if (equippedPrefab != null)
            {
                Transform handTransform = GetTransform(rightHand, leftHand);
                weapon = Instantiate(equippedPrefab, handTransform);
                weapon.gameObject.name = weaponName;
            }
            if (animatorOverride != null)
            {
                animator.runtimeAnimatorController = animatorOverride;
            }
            else
            {
                var overrideController = animator.runtimeAnimatorController as AnimatorOverrideController;
                if (overrideController != null)
                {
                    animator.runtimeAnimatorController = overrideController.runtimeAnimatorController;
                }
            }
            return weapon;
        }

        /// <summary>
        /// 消去角色手上拿的武器
        /// </summary>
        /// <param name="rightHand">角色的右手</param>
        /// <param name="leftHand">角色的左手</param>
        private void DestroyOldWeapon(Transform rightHand, Transform leftHand)
        {
            //確認角色手上拿的武器
            Transform oldWeapon = rightHand.Find(weaponName);
            if (oldWeapon == null)
            {
                //若右手沒有武器，則檢查左手有沒有。
                oldWeapon = leftHand.Find(weaponName);
            }
            if (oldWeapon == null)
            {
                //都沒有拿著武器的話，則不進行動作。
                return;
            }
            //替即將消失的武器重新命名，避免再次進入上面的選擇區段。
            oldWeapon.name = "DESTROYING";
            Destroy(oldWeapon.gameObject);
        }

        private Transform GetTransform(Transform rightHand, Transform leftHand)
        {
            Transform handTransform;
            if (isRightHanded)
            {
                handTransform = rightHand;
            }
            else
            {
                handTransform = leftHand;
            }
            return handTransform;
        }

        public bool HasProjectile()
        {
            return projectile != null;
        }

        public void LaunchProjectile(Transform rightHand, Transform leftHand, Health target, GameObject instigator, float calculatedDamage)
        {
            Projectile projectileInstance = 
                Instantiate(projectile, GetTransform(rightHand, leftHand).position, Quaternion.identity);
            projectileInstance.SetTarget(target, instigator, weaponDamage);
        }

        public float WeaponPercentageBonus
        {
            get
            {
                return percentageBonus;
            }
        }

        public float WeaponDamage
        {
            get
            {
                return weaponDamage;
            }
        }

        public float WeaponRange
        {
            get
            {
                return weaponRange;
            }
        }
    }
}
