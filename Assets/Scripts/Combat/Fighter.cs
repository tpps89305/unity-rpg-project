﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Movement;
using RPG.Core;
using System;
using RPG.Saving;
using RPG.Attributes;
using RPG.Stats;
using GameDevTV.Utils;

namespace RPG.Combat
{
    public class Fighter : MonoBehaviour, IAction, ISaveable, IModifierProvider
    {
        [SerializeField]
        float timeBetweenAttack = 1f;
        [Header("右手")]
        [SerializeField]
        Transform rightHandTransform = null;
        [Header("左手")]
        [SerializeField]
        Transform leftHandTransform = null;
        [SerializeField]
        WeaponConfig defaultWeapon = null;
        WeaponConfig currentWeaponConfig;
        LazyValue<Weapon> currentWeapon;

        private Health mTarget;
        float timeSinceLastAttack = Mathf.Infinity; //攻擊速度

        private void Awake()
        {
            currentWeaponConfig = defaultWeapon;
            currentWeapon = new LazyValue<Weapon>(SetupDefaultWeapon);
        }

        private Weapon SetupDefaultWeapon()
        {
            return AttachWeapon(defaultWeapon);
        }

        private void Start()
        {
            currentWeapon.ForceInit();
        }

        // Update is called once per frame
        void Update()
        {
            timeSinceLastAttack += Time.deltaTime;
            if (mTarget == null) return;
            if (mTarget.IsDead()) return;
            if (mTarget.GetComponent<Health>().IsDead()) return;
            if (!IsInRange())
            {
                GetComponent<Mover>().MoveTo(mTarget.transform.position, 1f);
                //Debug.Log("兩者之間的距離：" + Vector3.Distance(transform.position, targetTransform.position));
            }
            else
            {
                GetComponent<Mover>().Cancel();
                AttackBehaviour();
            }
        }

        public void EquipWeapon(WeaponConfig weaponConfig)
        {
            currentWeaponConfig = weaponConfig;
            currentWeapon.value = AttachWeapon(weaponConfig);
        }

        private Weapon AttachWeapon(WeaponConfig weapon)
        {
            Animator animator = GetComponent<Animator>();
            return weapon.Spawn(rightHandTransform, leftHandTransform, animator);
        }

        public Health GetTarget()
        {
            return mTarget;
        }

        /// <summary>
        /// 發動攻擊
        /// </summary>
        private void AttackBehaviour()
        {
            transform.LookAt(mTarget.transform);
            if (timeSinceLastAttack > timeBetweenAttack)
            {
                // 這個功能會觸發 Hit() 方法
                GetComponent<Animator>().SetTrigger("attack");
                timeSinceLastAttack = 0;
            }
        }

        /// <summary>
        /// 檢查人物是否在武器的攻擊範圍內
        /// </summary>
        /// <returns><c>true</c>, if in range was ised, <c>false</c> otherwise.</returns>
        private bool IsInRange()
        {
            return Vector3.Distance(transform.position, mTarget.transform.position) < currentWeaponConfig.WeaponRange;
        }

        /// <summary>
        /// 使角色停止攻擊動作
        /// </summary>
        public void Cancel()
        {
            StopAttack();
            mTarget = null;
            GetComponent<Mover>().Cancel();
        }

        private void StopAttack()
        {
            GetComponent<Animator>().SetTrigger("stopAttack");
            GetComponent<Animator>().ResetTrigger("stopAttack");
        }

        /// <summary>
        /// 使角色朝目標攻擊
        /// </summary>
        /// <param name="target">被選中的目標</param>
        public void Attack(GameObject target)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            // 抓到目標後，會在 Update 區段中進行追逐、攻擊動作。
            mTarget = target.GetComponent<Health>();
        }

        /// <summary>
        /// 播放動畫時，輸出傷害給對方。
        /// 由動畫(Animation)內的 Hit 事件觸發。
        /// </summary>
        public void Hit()
        {
            if (mTarget == null) return;

            float damage = GetComponent<BaseStats>().GetStat(Stat.Damage);

            if (currentWeapon.value != null)
            {
                currentWeapon.value.OnHit();
            }

            if (currentWeaponConfig.HasProjectile())
            {
                currentWeaponConfig.LaunchProjectile(rightHandTransform, leftHandTransform, mTarget, gameObject, damage);
            }
            else
            {
                mTarget.TakeDamage(gameObject, damage);
            }
            Debug.Log(String.Format("{0} 對 {1} 造成 {2} 點傷害", gameObject.name, mTarget.name, damage));
        }

        /// <summary>
        /// 由動畫(Animation)內的 Shoot 事件觸發。
        /// </summary>
        public void Shoot()
        {
            Hit();
        }

        public bool CanAttack(GameObject combatTarget)
        {
            if (combatTarget == null) return false;
            // 確定敵方也在玩家可行動的範圍內，才進行動作。
            if (!GetComponent<Mover>().CanMoveTo(combatTarget.transform.position)) { return false; }
            Health targetToTest = combatTarget.GetComponent<Health>();
            return targetToTest != null && targetToTest.GetComponent<Health>().GetHealthPoint() > 0;
        }

        public object CaptureState()
        {
            Debug.Log("Fighter CaptureState() " + currentWeaponConfig.name);
            return currentWeaponConfig.name;
        }

        public void RestoreState(object state)
        {
            string weaponName = (string)state;
            Debug.Log("Fighter RestoreState() " + weaponName);
            WeaponConfig weapon = UnityEngine.Resources.Load<WeaponConfig>(weaponName);
            EquipWeapon(weapon);
        }

        /// <summary>
        /// 武器數值，利用迭代器使用這個方法。
        /// </summary>
        /// <returns>這件武器的攻擊力</returns>
        /// <param name="stat">在這裡固定為 Stat.Damage</param>
        public IEnumerable<float> GetAdditiveModifiers(Stat stat)
        {
            if (stat == Stat.Damage)
            {
                yield return currentWeaponConfig.WeaponDamage;
            }
        }

        public IEnumerable<float> GetPercentageModifiers(Stat stat)
        {
            if (stat == Stat.Damage)
            {
                yield return currentWeaponConfig.WeaponPercentageBonus;
            }
        }
    }
}
