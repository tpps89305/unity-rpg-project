﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Core;
using RPG.Attributes;
using UnityEngine.Events;

namespace RPG.Combat
{
    public class Projectile : MonoBehaviour
    {
        Health target = null;
        GameObject instigator = null;
        float damage = 10f;

        [SerializeField]
        float speed = 1f;
        [SerializeField]
        bool isHoming = true;
        [SerializeField]
        GameObject hitEffect = null;
        [SerializeField]
        float maxLifeTime = 10f;
        [SerializeField]
        GameObject[] destoryOnHit = null;
        [SerializeField]
        float lifeAfterImpace = 2f;
        [SerializeField]
        UnityEvent onHit;

        private void Start()
        {
            transform.LookAt(GetAimLocation());
        }

        // Update is called once per frame
        void Update()
        {
            if (isHoming)
            {
                transform.LookAt(GetAimLocation());
            }
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }

        public void SetTarget(Health target, GameObject instigator, float weaponDamage)
        {
            this.target = target;
            this.damage = weaponDamage;
            this.instigator = instigator;
            Destroy(gameObject, maxLifeTime);
        }

        private Vector3 GetAimLocation()
        {
            CapsuleCollider targetCapsule = target.GetComponent<CapsuleCollider>();
            if (targetCapsule == null)
            {
                return target.transform.position;
            }
            return target.transform.position + Vector3.up * targetCapsule.height / 2f;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Health>() != target) return;
            if (target.IsDead()) return;
            target.TakeDamage(instigator, damage);
            onHit.Invoke();
            speed = 0;
            if (hitEffect != null)
            {
                Instantiate(hitEffect, GetAimLocation(), transform.rotation);
            }

            foreach (GameObject onDestroy in destoryOnHit)
            {
                Destroy(onDestroy);
            }

            Destroy(gameObject, lifeAfterImpace);
        }
    }
}
