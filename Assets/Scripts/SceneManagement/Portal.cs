﻿using System;
using System.Collections;
using RPG.Control;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace RPG.SceneManagement
{
    public class Portal : MonoBehaviour
    {

        enum DestinationIndentifier
        {
            Village, Lane
        }

        [SerializeField]
        [Header("要讀取的場景代號")]
        int sceneToLoad = -1;

        [SerializeField]
        [Header("角色進入點")]
        Transform spawnPoint;

        [SerializeField]
        [Header("淡出時間")]
        float fadeOutTime = 1f;

        [SerializeField]
        [Header("淡入時間")]
        float fadeInTime = 1f;

        [SerializeField]
        [Header("目的地")]
        DestinationIndentifier destination;

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log(other.tag + "進入傳送門");
            if (other.tag == "Player")
            {
                Debug.Log("進入了傳送門");
                StartCoroutine(Transition());
            }
        }

        private IEnumerator Transition()
        {
            if (sceneToLoad < 0)
            {
                Debug.LogError("尚未設定要讀取的場景");
                yield break;
            }

            PlayerController playerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            playerController.enabled = false;

            Fader fader = FindObjectOfType<Fader>();
            yield return fader.FadeOut(fadeOutTime);

            SavingWrapper savingWrapper = FindObjectOfType<SavingWrapper>();
            savingWrapper.Save();
            DontDestroyOnLoad(gameObject); //避免在切換場景時，因物件被清除導致角色定位(UpdatePlayer)失敗。
            yield return SceneManager.LoadSceneAsync(sceneToLoad);

            // 讀取新場景後，要重新尋找物件。
            PlayerController newPlayerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            newPlayerController.enabled = false;

            Debug.Log("場景讀取完成");
            Portal otherPortal = GetOtherPortal();
            savingWrapper.Load();
            UpdatePlayer(otherPortal);
            savingWrapper.Save();
            yield return fader.FadeIn(fadeInTime);

            newPlayerController.enabled = true;
            Destroy(gameObject); //角色定位(UpdatePlayer)完畢後，清除這個物件。
        }

        private void UpdatePlayer(Portal otherPortal)
        {
            GameObject player = GameObject.FindWithTag("Player");
            player.GetComponent<NavMeshAgent>().enabled = false;
            player.transform.position = otherPortal.spawnPoint.position;
            player.transform.rotation = otherPortal.spawnPoint.rotation;
            player.GetComponent<NavMeshAgent>().enabled = true;
            Debug.Log("Player 的位置是 " + player.transform.position + "，Spawn Point 的位置是 " + otherPortal.spawnPoint.position);
        }

        private Portal GetOtherPortal()
        {
            foreach (Portal portal in FindObjectsOfType<Portal>())
            {
                // 找到與之配對的傳送門
                if (portal == this) continue;
                if (portal.destination != destination) continue;
                return portal;
            }
            return null;
        }
    }
}
