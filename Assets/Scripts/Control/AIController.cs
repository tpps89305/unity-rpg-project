﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Utils;
using RPG.Combat;
using RPG.Core;
using RPG.Movement;
using RPG.Attributes;
using UnityEngine;

namespace RPG.Control
{
    public class AIController : MonoBehaviour
    {
        [SerializeField]
        float chaseDistance = 5f; //追逐範圍
        [SerializeField]
        float suspicionTime = 3f;
        [SerializeField]
        float agroCooldownTime = 5f;
        [SerializeField]
        PatrolPath patrolPath;
        [SerializeField]
        float wayPointTolerance = 1f;
        [SerializeField]
        float wayPointDwellTime = 3f;
        [SerializeField]
        [Range(0, 1)]
        float patrolSpeedFration = 0.2f;
        [SerializeField]
        float shoutDistance = 50f; // 呼救距離

        Fighter fighter;
        GameObject player;
        Health health;
        Mover mover;

        //Vector3 guardPosition;
        LazyValue<Vector3> guardPosition;
        float timeSinceLastSawPlayer = Mathf.Infinity;
        float timeSinceArrivedWayPoint = Mathf.Infinity;
        float timeSinceAggravated = Mathf.Infinity;
        int currentWayPointIndex = 0;

        private void Awake()
        {
            fighter = GetComponent<Fighter>();
            player = GameObject.FindWithTag("Player");
            health = GetComponent<Health>();
            mover = GetComponent<Mover>();
            guardPosition = new LazyValue<Vector3>(GetGuardPosition);
        }

        private Vector3 GetGuardPosition()
        {
            return transform.position;
        }

        // Start is called before the first frame update
        void Start()
        {
            guardPosition.ForceInit();
        }

        // Update is called once per frame
        void Update()
        {
            if (health.IsDead()) return;
            //若玩家在追逐範圍內，就展開追逐。
            if (IsAggravated() && fighter.CanAttack(player))
            {
                AttackBehaviour();
            }
            else if (timeSinceLastSawPlayer < suspicionTime)
            {
                GetComponent<ActionScheduler>().CancelCurrentAction();
            }
            else
            {
                PatrolBahaviour();
            }
            timeSinceLastSawPlayer += Time.deltaTime;
            timeSinceArrivedWayPoint += Time.deltaTime;
            timeSinceAggravated += Time.deltaTime;
        }

        private void AttackBehaviour()
        {
            timeSinceLastSawPlayer = 0;
            fighter.Attack(player);
            AggravateNearbyEnemies();
        }

        /// <summary>
        /// 當敵人被激怒時，在以自己為中心，呼叫方圓一定距離內的人員一起攻擊玩家。
        /// 呼叫距離由參數 <code>shoutDistance</code> 決定。
        /// </summary>
        private void AggravateNearbyEnemies()
        {
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, shoutDistance, Vector3.up, 0);
            foreach(RaycastHit hit in hits)
            {
                AIController ai = hit.collider.GetComponent<AIController>();
                if (ai == null) continue;
                ai.Aggravate();
            }
        }

        private void PatrolBahaviour()
        {
            Vector3 nextPosition = guardPosition.value;
            if (patrolPath != null)
            {
                if (AtWayPoint())
                {
                    timeSinceArrivedWayPoint = 0;
                    CycleWayPoint();
                }
                nextPosition = GetCurrentWayPoint();
            }
            if (timeSinceArrivedWayPoint > wayPointDwellTime)
            {
                mover.StartMoveAction(nextPosition, patrolSpeedFration);
            }
        }

        private Vector3 GetCurrentWayPoint()
        {
            return patrolPath.GetWayPoint(currentWayPointIndex);
        }

        private void CycleWayPoint()
        {
            currentWayPointIndex = patrolPath.GetNextIndex(currentWayPointIndex);
        }

        private bool AtWayPoint()
        {
            float distanceToWayPoint = Vector3.Distance(transform.position, GetCurrentWayPoint());
            return distanceToWayPoint < wayPointTolerance;
        }

        /// <summary>
        /// 偵測玩家是否在自己的攻擊範圍內
        /// </summary>
        /// <returns>是的話回傳 <code>true</code></returns>
        private bool IsAggravated()
        {
            float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
            return distanceToPlayer < chaseDistance || timeSinceAggravated < agroCooldownTime;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);
        }

        public void Aggravate()
        {
            timeSinceAggravated = 0;
        }
    }
}