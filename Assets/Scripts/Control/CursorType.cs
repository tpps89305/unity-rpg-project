﻿namespace RPG.Control
{
    public enum CursorType
    {
        None,
        Movement,
        Combat,
        OverUI,
        PickUp
    }
}
