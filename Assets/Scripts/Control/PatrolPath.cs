﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    public class PatrolPath : MonoBehaviour
    {

        private void OnDrawGizmos()
        {
            for(int i = 0; i < transform.childCount; i++)
            {
                int j = GetNextIndex(i);
                Gizmos.DrawSphere(GetWayPoint(i), 0.3f);
                Gizmos.DrawLine(GetWayPoint(i), GetWayPoint(j));
            }
        }

        /// <summary>
        /// 取得下一個點的 Index
        /// </summary>
        /// <returns>下一個 Index，即為 i + 1，若回到原點就會得到 0（初始值）。</returns>
        /// <param name="i">迴圈中現在的 Index</param>
        public int GetNextIndex(int i)
        {
            if (i + 1 == transform.childCount)
            {
                return 0;
            }
            else
            {
                return i + 1;
            }
        }

        public Vector3 GetWayPoint(int index)
        {
            return transform.GetChild(index).position;
        }
    }
}
