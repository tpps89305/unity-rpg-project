﻿using System;
using System.Collections;
using GameDevTV.Utils;
using RPG.Attributes;
using UnityEngine;

namespace RPG.Stats
{
    public class BaseStats : MonoBehaviour
    {
        [Range(1, 10)]
        [SerializeField]
        int startingLevel = 1;
        [SerializeField]
        CharacterClass characterClass;
        [SerializeField]
        Progression progression = null;
        [SerializeField]
        GameObject levelUpParticleEffect = null;
        [SerializeField]
        bool shouldUseModifiers = false;

        public event Action onLevelUp;
        //int currentLevel = 1;
        LazyValue<int> currentLevel;
        Experience experience;

        private void Awake()
        {
            experience = GetComponent<Experience>();
            currentLevel = new LazyValue<int>(CalculateLevel);
        }

        private void Start()
        {
            currentLevel.value = CalculateLevel();
        }

        private void OnEnable()
        {
            if (experience != null)
            {
                experience.onExperienceGained += UpdateLevel;
            }
        }

        private void OnDisable()
        {
            if (experience != null)
            {
                experience.onExperienceGained -= UpdateLevel;
            }
        }

        private void UpdateLevel()
        {
            int newLevel = CalculateLevel();
            if (newLevel > currentLevel.value)
            {
                currentLevel.value = newLevel;
                print("Level Up!");
                ShowLevelUpEffect();
                onLevelUp();
            }
        }

        private void ShowLevelUpEffect()
        {
            Instantiate(levelUpParticleEffect, transform);
        }

        private void Update()
        {
            // 測試用
            //if (gameObject.tag == "Player")
            //{
            //    Debug.Log("GetLevel = " + GetLevel());
            //}
        }

        /// <summary>
        /// 依據角色的等級，設定角色的數值。
        /// </summary>
        /// <returns>角色的數值。這個數值就是由 Progression 決定。</returns>
        public float GetStat(Stat stat)
        {
            return (GetBaseStat(stat) + GetAdditiveModifier(stat)) * (1 + GetPercentageModifier(stat) / 100);
        }

        private float GetPercentageModifier(Stat stat)
        {
            if (!shouldUseModifiers) return 0;

            float total = 0;
            foreach(IModifierProvider provider in GetComponents<IModifierProvider>())
            {
                foreach(float modifiers in provider.GetPercentageModifiers(stat))
                {
                    total += modifiers;
                }
            }
            return total;
        }

        private float GetBaseStat(Stat stat)
        {
            return progression.GetStat(stat, characterClass, currentLevel.value);
        }

        public int GetLevel()
        {
            if (currentLevel.value < 1)
            {
                currentLevel.value = CalculateLevel();
            }
            return currentLevel.value;
        }

        private float GetAdditiveModifier(Stat stat)
        {
            if (!shouldUseModifiers) return 0;

            float total = 0;
            foreach(IModifierProvider provider in GetComponents<IModifierProvider>())
            {
                foreach(float modifiers in provider.GetAdditiveModifiers(stat))
                {
                    total += modifiers;
                }
            }
            return total;
        }

        private int CalculateLevel()
        {
            if (experience == null)
            {
                return startingLevel;
            }
            float currentXP = experience.GetPoint();
            int penultimateLevel = progression.GetLevels(Stat.ExperienceToLevelUp, characterClass);
            for (int levels = 1; levels <= penultimateLevel; levels++)
            {
                float xpToLevelUp = progression.GetStat(Stat.ExperienceToLevelUp, characterClass, levels);
                if(xpToLevelUp > currentXP)
                {
                    return levels;
                }
            }
            return penultimateLevel + 1;
        }
    }
}
